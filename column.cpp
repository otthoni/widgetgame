#include "column.hpp"
#include "matrica.hpp"
#include "graphics.hpp"
#include "ablak.hpp"
#include <iostream>
#include<algorithm>
#include<cmath>
using namespace genv;
using namespace std;

Column::Column(Ablak *a,int u, int v): Matrica(a,u,v,100,v*100), _pos(u/100){
    a->add_matrica(this);
    T1=new canvas();
    T2=new canvas();
    T3=new canvas();
    T1->open(_sx,_sx);
    T2->open(_sx,_sx);
    T3->open(_sx,_sx);
    _r=40;
    for(int j=0; j<_sx; j++){
        for(int i=0; i<_sx; i++){
            if(pow(i-50,2) + pow(j-50,2) <= pow(_r,2)){
            *T1 << move_to(i, j) << color(255,255,255) << dot;
            *T2 << move_to(i, j) << color(255,255,0) << dot;
            *T3 << move_to(i, j) << color(255,0,0) << dot;
            }
        }
    }
    T1->transparent(1);
    T2->transparent(1);
    T3->transparent(1);
    for(int i=0; i<_y; i++){
        _tokens.insert(_tokens.end(), T1);
        _placed.insert(_placed.end(), 0);
    }

}


 void Column::rajzol(){
    for(int i=1; i<=_tokens.size(); i++){
        gout << stamp(*_tokens[i-1],_x, _sy-i*_sx);
    }
}

vector<bool> Column::place(bool in_turn){
    for(int i=0; i<_placed.size(); i++){
                if(!_placed[i]){
                    _placed[i]=1;
                    if(in_turn)
                    _tokens[i]=T2;
                    else
                    _tokens[i]=T3;
                    rajzol();
                    break;
                }
            }
    if(in_turn) return convert(_placed, T2);
    else return convert(_placed, T3);
}

int Column::position(){
    return _pos;
}

bool Column::contains(int y){
    return _placed[y];
}

bool Column::full(){
    return find(_placed.begin(), _placed.end(), 0) == _placed.end();
}

vector<bool> Column::convert(vector<bool> p, canvas *T){
    for(int i=0; i<p.size(); i++){
        if(_tokens[i]==T) 
            p[i]=1;
            else
            p[i]=0;
    }
    return p;
}

 bool Column::ezt(int ex, int ey){
    return ex >= _x && ex <= _x + _sx;
}

void Column::reset(){
    _placed=vector<bool>(_placed.size(),0);
    _tokens=vector<canvas*>(_tokens.size(), T1);
}


