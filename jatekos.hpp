#ifndef JATEKOS_HPP_INCLUDED
#define JATEKOS_HPP_INCLUDED
#include <vector>
class Matrica;
class Ablak;
class Jatekos{
protected:
    bool _order;
    std::vector<std::vector<bool>> _placed;
public:
    Jatekos(Ablak *a, bool o, int c, int l);
    virtual void move(Matrica *c);
    virtual std::vector<std::vector<bool>> state();
    virtual void reset();
    virtual ~Jatekos(){};
};
#endif
