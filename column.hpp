#ifndef COLUMN_HPP_INCLUDED
#define COLUMN_HPP_INCLUDED
#include "matrica.hpp"
#include "graphics.hpp"
#include <vector>
#include <string>
class Ablak;
struct Column: public Matrica
{
protected:
    std::vector<bool> _placed;
    genv::canvas *T1;
    genv::canvas *T2;
    genv::canvas *T3;
    std::vector<genv::canvas*> _tokens;
    int _r;
    int _pos;
public:
    Column(Ablak *a,int u, int v);
    virtual void rajzol() override;
    virtual bool ezt(int ex, int ey) override;
    virtual int position() override;
    virtual std::vector<bool> place(bool in_turn) override;
    virtual bool full() override;
    bool contains(int y);
    void reset();
    std::vector<bool> convert(std::vector<bool> p, genv::canvas *T); 

};


#endif // Column_HPP_INCLUDED
