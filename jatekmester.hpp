#ifndef JATEKMESTER_HPP_INCLUDED
#define JATEKMESTER_HPP_INCLUDED
#include "ablak.hpp"
#include<vector>
#include <queue>
class Column;
class Jatekos;
class Ellenfel;

struct Nav{
    int cost;
    int x;
    bool operator>(const Nav& other) const{                                   
        return cost > other.cost;                                                     
    } 
};

class JatekMester : public Ablak
{
    std::vector<Column*> _columns;
    std::vector<std::vector<bool>> s_A, s_B;
    std::priority_queue<Nav, std::vector<Nav>, std::greater<Nav>> q;
    Jatekos *A;
    Ellenfel *B;
    bool _connected;
    int _to_connect;


public:
    JatekMester(int x, int y, int n);
    void game();
    virtual void update() override;
    virtual bool full() override;
    virtual void reset() override;
    virtual bool connected() override;
    virtual void enemy_move() override;
    bool guaranteed(int x, int y);
    Nav estimate_a(bool t, int& an, int x, int y, int i);
    Nav estimate_b(bool t, int& bn, int x, int y, int i);
    void connect(bool t, int& n, int i, int j, int d);
};

#endif
