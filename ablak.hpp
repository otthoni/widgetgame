#ifndef Ablak_HPP_INCLUDED
#define Ablak_HPP_INCLUDED
#include<vector>
class Matrica;
class Jatekos;
struct Ablak
{
protected:
    int XX, YY;
    std::vector<Matrica*> _columns;
    std::vector<Jatekos*> Players;
public:
    Ablak(int x, int y);
    void add_matrica(Matrica *m);
    void add_player(Jatekos *p);
    void event_loop();
    virtual void update()=0;
    virtual bool full()=0;
    virtual void reset()=0;
    virtual bool connected()=0;
    virtual void enemy_move()=0;
    void print_draw();
    void print_victory(bool in_turn);
};

#endif // Ablak_HPP_INCLUDED
