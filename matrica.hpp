#ifndef Matrica_HPP_INCLUDED
#define Matrica_HPP_INCLUDED
#include "graphics.hpp"
#include <string>
#include <vector>
class Ablak;
struct Matrica
{
protected:
    int _x, _y;
    int _sx, _sy;
public:
    Matrica(Ablak *a,int x, int y, int sx, int sy);
    virtual void rajzol()=0;
    virtual bool ezt(int ex, int ey)=0;
    virtual int position()=0;
    virtual std::vector<bool> place(bool in_turn)=0;
    virtual bool full()=0;
    virtual ~Matrica() {}

};

#endif
