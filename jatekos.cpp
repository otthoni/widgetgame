#include "jatekos.hpp"
#include "ablak.hpp"
#include "matrica.hpp"
#include<functional>
#include <vector>
using namespace std;
Jatekos::Jatekos(Ablak *a, bool o, int c, int l): _order(o), _placed(vector<vector<bool>>(c, vector<bool>(l,0))){
    a->add_player(this);
};

void Jatekos::move(Matrica *c){
   _placed[c->position()] = c->place(_order);
};

vector<vector<bool>> Jatekos::state(){
    return _placed;
}

void Jatekos::reset(){
    _placed=vector<vector<bool>>(_placed.size(), vector<bool>(_placed[0].size(), 0));
}