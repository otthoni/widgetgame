#include "ablak.hpp"
#include "column.hpp"
#include "jatekmester.hpp"
#include "jatekos.hpp"
#include "ellenfel.hpp"
#include<vector>
#include <fstream>
#include<iostream>
using namespace std;



JatekMester::JatekMester(int x, int y, int n) : Ablak(100*x,100*y), _to_connect(n), _connected(0){
    for(int i=0; i<x; i++)
    {
        Column *c = new Column(this, i*100, y);
        _columns.insert(_columns.end(), c);
    }

    A = new Jatekos(this,0, x, y);
    B = new Ellenfel(this,1, x, y);
   
};

void JatekMester::game(){
    event_loop();
}

void JatekMester::update(){
    s_A=A->state();
    s_B=B->state();
    for(int i=0; i<s_A.size(); i++){
        for(int j=0; j<s_A[i].size(); j++){
            if(s_B[i][j])
                 for(int d=1; d<=8; d++){
                        int num=1;
                        connect(s_B[i][j],num, i, j, d);
                    }
            if(s_A[i][j]) 
                for(int d=1; d<=8; d++){
                        int num=1;
                        connect(s_A[i][j],num, i, j, d);
                    }
        }
    }
    /*for(int i=0; i<s_B.size(); i++){
        for(int j=s_B[i].size(); j!=0; j--){
            cout << s_A[i][j-1] << endl << flush;
        }
        cout << endl << flush;
    }
    cout << "-------" << endl << flush;
 */   
}

void JatekMester::enemy_move(){
    s_A = A->state();
    s_B = B->state();
	for(int i=0; i<s_A.size(); i++){
		for(int j=0; j<s_A[0].size(); j++){
            if(s_B[i][j]){
				for(int d=1; d<=8; d++){
                    int num=0;
                    Nav b = estimate_b(s_B[i][j],num, i, j, d);
					q.push(b);
                }
            }
            if(s_A[i][j]){                    // elvileg számít a queue azonos értékű elemeinek behelyezési sorrendje, tehát a botot nem lehet eltántorítani a győzelemtől az utolsó lépésben
				for(int d=1; d<=8; d++){
                    int num=0;
                    Nav a = estimate_a(s_A[i][j],num, i, j, d);
					if(a.cost == 1 ){q.push(a);}        // itt lehetséges a nehezítés
                }
            }
		}
	}
        
	if(!B->empty() && !q.empty()){	
        while(_columns[q.top().x]->full()){
            q.pop();
        }
        B->move(_columns[q.top().x]);   // igazából a költségek alapján el lehetne dönteni, hogy nyert-e valaki, és megspórolnánk az update fv ciklusait
        q.pop();
        while(!q.empty()) q.pop();
        }
        else{
            B->move(_columns[0]);
        }
}



Nav JatekMester::estimate_a(bool t, int& an, int x, int y, int i) {
    int new_x = x;
    int new_y = y;

    if (i == 1 || i == 4 || i == 6) {
        new_x--;
    } else if (i == 3 || i == 5 || i == 8) {
        new_x++;
    }

    if (i == 1 || i == 2 || i == 3) {
        new_y--;
    } else if (i == 6 || i == 7 || i == 8) {
        new_y++;
    }

    Nav a;
    if (new_x >= 0 && new_x < s_A.size() && new_y >= 0 && new_y < s_A[0].size() && s_A[x][y] && !s_B[new_x][new_y] && guaranteed(new_x,new_y)) {
            an++;
            a = estimate_a(s_A[new_x][new_y], an, new_x, new_y, i);
        } 
    else{
        a = {_to_connect - an, x};
    }
    return a;
}

Nav JatekMester::estimate_b(bool t, int& bn, int x, int y, int i) {
    int new_x = x;
    int new_y = y;

    if (i == 1 || i == 4 || i == 6) {
        new_x--;
    } else if (i == 3 || i == 5 || i == 8) {
        new_x++;
    }

    if (i == 1 || i == 2 || i == 3) {
        new_y--;
    } else if (i == 6 || i == 7 || i == 8) {
        new_y++;
    }

    Nav b;
    if (new_x >= 0 && new_x < s_B.size() && new_y >= 0 && new_y < s_B[0].size() && s_B[x][y] && !s_A[new_x][new_y] && guaranteed(new_x,new_y)){
            bn++;
            b = estimate_b(s_B[new_x][new_y], bn, new_x, new_y, i);
        }
        else {
            b = {_to_connect - bn, x};
        }
    return b;
}


bool JatekMester::guaranteed(int x, int y){
    if(y >= 1){
        if(x >= 0 && x <= _columns.size() && _columns[x]->contains(y-1)) return true;
        else
        return false;
    }
    else
    return true;
}



void JatekMester::connect(bool current, int& n, int x, int y, int i){
    int new_x = x;
    int new_y = y;

        if (i == 1 || i == 4 || i == 6) {
            new_x--;
        } else if (i == 3 || i == 5 || i == 8) {
            new_x++;
        }

        if (i == 1 || i == 2 || i == 3) {
            new_y--;
        } else if (i == 6 || i == 7 || i == 8) {
            new_y++;
        }

        if (new_x >= 0 && new_x < s_A.size() && new_y >= 0 && new_y < s_A[0].size()) {
          if(s_A[x][y] && s_A[new_x][new_y]){
           n++;
            if(n<_to_connect){
                connect(s_A[new_x][new_y], n, new_x, new_y, i);
            }
            else{
            _connected=true;
            return; 
            }
          }
            if(s_B[x][y] && s_B[new_x][new_y]){
                n++;
                if(n<_to_connect){
                    connect(s_B[new_x][new_y], n, new_x, new_y, i);
                }
                else {
                _connected=true;
                return;
                }
            }
        }
}


bool JatekMester::connected(){
    return _connected;
}

bool JatekMester::full(){
    for(auto c : _columns){
        if(!c->full())
            return false;
    }
    return true;
}

void JatekMester::reset(){
    for(auto c : _columns){
        c->reset();
    }
    A->reset();
    B->reset();
    _connected=false;
}


