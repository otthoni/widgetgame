#ifndef ELLENFEL_HPP_INCLUDED
#define ELLENFEL_HPP_INCLUDED
#include "jatekos.hpp"
#include<vector>
class Matrica;
class Ablak;
class Ellenfel : public Jatekos{
protected:
    std::vector<std::vector<bool>>_placed;
public:
    Ellenfel(Ablak *a, bool o, int c, int l);
    void move(Matrica *c);
    std::vector<std::vector<bool>> state();
    void reset();
    bool empty();
};

#endif
