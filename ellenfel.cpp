#include "ellenfel.hpp"
#include "jatekos.hpp"
#include "matrica.hpp"
#include <vector>
#include <iostream>
#include <algorithm>
#include <thread>
#include <chrono>
class Ablak;
using namespace std;

    Ellenfel::Ellenfel(Ablak *a, bool o, int c, int l) : Jatekos(a, o, c, l), _placed(vector<vector<bool>>(c, vector<bool>(l, 0))) {
    }

    void Ellenfel::move(Matrica *c){
        //this_thread::sleep_for(chrono::milliseconds(300));      
        _placed[c->position()] = c->place(_order);
    }

    bool Ellenfel::empty(){
        for(int i=0; i<_placed.size(); i++){
            if(find(_placed[i].begin(), _placed[i].end(), 1) != _placed[i].end()) return false;
        }
        return true;
    }

    vector<vector<bool>> Ellenfel::state(){
    return _placed;
    }

    void Ellenfel::reset(){
    _placed=vector<vector<bool>>(_placed.size(), vector<bool>(_placed[0].size(), 0));
    }


