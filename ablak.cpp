#include "ablak.hpp"
#include "matrica.hpp"
#include "graphics.hpp"
#include "jatekos.hpp"
#include<vector>
#include<iostream>
using namespace std;
using namespace genv;


Ablak::Ablak(int x, int y) : XX(x), YY(y)
{
    gout.open(XX,YY);
}

void Ablak::add_matrica(Matrica *m){
    _columns.push_back(m);
}

void Ablak::add_player(Jatekos *p){
    Players.push_back(p);
}

void Ablak::print_draw(){
    string d="Döntetlen!";
    string s="Új játék: Enter";
    gout << font("LiberationSans-Regular.ttf",100);
    gout << move_to((XX-gout.twidth(d))/2, YY/3-60) << color(250,150,0) << text(d)
    << font("LiberationSans-Regular.ttf",50) << move_to((XX-gout.twidth(s))/2, YY/3+70) << color(250,150,0) << text(s) << refresh;
}

void Ablak::print_victory(bool in_turn){
    string d;
    string s="Új játék: Enter";
    if(in_turn){
    d = "Piros nyert!!";
    }else{
    d="Sárga nyert!!";
    }
    gout << font("LiberationSans-Regular.ttf",100);
    gout << move_to((XX-gout.twidth(d))/2, YY/3-60) << color(250,150,0) << text(d)
    << font("LiberationSans-Regular.ttf",50) << move_to((XX-gout.twidth(s))/2, YY/3+70) << color(250,150,0) << text(s) << refresh;
}

void Ablak::event_loop(){
    bool red=0;
    event ev;
    int fokuszban = -1;
    while(gin >> ev && ev.keycode!=key_escape ) {
        gout << move_to(0,0) << color(0,48,102) << box(XX,YY);
        for (Matrica * m : _columns) {
            m->rajzol();
        }
        gout << refresh;
        while(connected() && gin >> ev){
            print_victory(red);
            if(ev.keycode==key_enter){
                 reset();
                 red=0;
            }
            if(ev.keycode==key_escape) return;
        }
        while(full() && gin >> ev){
            print_draw();
            if(ev.keycode==key_enter) reset();
            if(ev.keycode==key_escape) return;
        }
        if (ev.type == ev_mouse && ev.button==btn_left) {
            for (size_t i=0;i<_columns.size();i++) {
                if (_columns[i]->ezt(ev.pos_x, ev.pos_y)) {
                        fokuszban = i; 
                }
            
            }
            if(fokuszban != -1 && !_columns[fokuszban]->full()){
                Players[0]->move(_columns[fokuszban]);
                gout << refresh;
                update();
                
                if(connected()){
                    red=1;
                    continue;
                }
                enemy_move();
            }
            update();
        }
    }
}

